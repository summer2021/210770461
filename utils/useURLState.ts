import { useState } from "react"

export default function useURLState<T>(
  initialValue: T,
  key: string
): [T, (newValue: T) => void] {
  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      // 通过key从本地存储拿到
      const queryString = window.location.search
      const params = new URLSearchParams(queryString)
      const item = params.get(key)
      // 解析设置好的JSON，如果没有就返回初始值
      return item ? JSON.parse(item) : initialValue
    } catch (error) {
      // 报错返回初始值
      console.error(error)
      return initialValue
    }
  })

  const setValue = (value: T | ((val: T) => T)) => {
    try {
      const valueToStore =
        value instanceof Function ? value(storedValue) : value
      // 保存状态
      setStoredValue(valueToStore)
      // 保存到URL query string
      const params = new URLSearchParams()
      params.set(key, JSON.stringify(valueToStore))
      window.history.replaceState(
        "",
        "",
        `${window.location.href.split("?")[0]}?${params.toString()}`
      )
    } catch (error) {
      console.error(error)
    }
  }

  return [storedValue, setValue]
}
