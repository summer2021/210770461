//从nextjs中引入dynamic组件,使应用具备动态加载能力（有需要再显示）
import dynamic from "next/dynamic"
import Layout from "../components/Layout"
import Head from "next/head"

//动态导入component中的ItemList组件
const ItemList = dynamic(() => import("../components/ItemList"), { ssr: false })

export default function HomePage() {
  return (
    <Layout>
      <Head>
        <title>Pickie</title>
      </Head>
      <ItemList />
    </Layout>
  )
}
